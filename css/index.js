let x; 
let y;
let z;

x = 6; 
y = 14;
z = 4;

document.write(`За замовчуванням x = ${x} <br>`);
document.write(`За замовчуванням y = ${y} <br>`);
document.write(`За замовчуванням z = ${z} <br><br>`);

document.write(`Перше рівняння: <br> x += y - x++ * z <br>  Результат : ${x += y - x++ * z} <br>`);
/*
     '*' має приорітет 13, а ++ приорітет 16, тож перше буде множення ( 4*6 = 24), а ++ стоять з правого боту, тож інкримент буде на наступній операції
    Далі вже буде складнання з присвоюванєм ( 14 - 24 = -10 ==> х = -10 + 6 = -4 )
*/
document.write(` * має приорітет 13, а ++ приорітет 16, тож перше буде множення ( 4*6 = 24), а ++ стоять з правого боту, тож інкримент буде на наступній операції <br>
Далі вже буде складнання з присвоюванєм ( 14 - 24 = -10 ==> х = -10 + 6 = -4 )  <br><br>`);

x = 6; 
y = 14;
z = 4;
document.write(`Друге рівняння: <br> z = --x - y * 5 <br>  Результат : ${z = --x - y * 5} <br>`);
/*
    Перше по пріорітету множення, далі відбуваеться декримент Хб а потім віднемання і останнєм присвоення
*/
document.write(` Перше по пріорітету множення, далі відбуваеться декримент Х а потім віднемання і останнєм присвоення
 <br><br>`);

x = 6; 
y = 14;
z = 4;
document.write(`Третэ рівняння: <br> y /= x + 5 % z  <br>  Результат : ${y /= x + 5 % z} <br>`);
/*
    Перше по пріорітету вираховуеться остаток від ділення ( 5 / 4 = 1 + 1 остаток)
    Потім буде додавання ( 6 + 1 = 7 ) і ділення у на результат ( 14 / 7 = 2 )
*/
document.write(`    Перше по пріорітету вираховуеться остаток від ділення ( 5 / 4 = 1 + 1 остаток)<br>
Потім буде додавання ( 6 + 1 = 7 ) і ділення У на результат ( 14 / 7 = 2 ) <br><br>`);

x = 6; 
y = 14;
z = 4;
document.write(`Четверте рівняння: <br> z - x++ + y * 5 <br>  Результат : ${z - x++ + y * 5} <br>`);
/*
Спочатку буде множення, а ++ стоять з правого боту, тож інкримент буде на наступній операції 
4 - 6 + 70 = 68 
*/
document.write(`Спочатку буде множення, а ++ стоять з правого боту, тож інкримент буде на наступній операції <br>
4 - 6 + 70 = 68 <br><br>`);

x = 6; 
y = 14;
z = 4;
document.write(`Пьяте  рівняння: <br>  x = y - x++ * z <br>  Результат : ${x = y - x++ * z} <br>`);
/*
     '*' має приорітет 13, а ++ приорітет 16, тож перше буде множення ( 4*6 = 24), а ++ стоять з правого боту, тож інкримент буде на наступній операції
     x = 14 - 24 = -10 
*/
document.write(`* має приорітет 13, а ++ приорітет 16, тож перше буде множення ( 4*6 = 24), а ++ стоять з правого боту, тож інкримент буде на наступній операції
x = 14 - 24 = -10  <br><br>`);



let zirka = "&nbsp *";
let probil = "&nbsp&nbsp"; 


for (let i = 0; i < 6; i++ ) {
    for(let j = 16; j > 0; j--){
        if( i == 0 || i == 5){
            document.write(zirka);
        }
        if( i > 0 && i < 5 && j == 16){
            document.write(zirka);
        }
        if( i > 0 && i < 5 && j < 15 ){
            document.write(probil + probil);
        }
        if( i > 0 && i < 5 && j == 1){
            document.write(zirka);
        }
    }
    document.write(`<br>`)
}
document.write(`<br>`);


for (let i = 0; i < 10; i++ ) {
    for(let j = i + 1 ; j < 10 ; j++){
        document.write(probil) 
    }
    for(let y = i + 1 ; y  > 0  ; y--){
        document.write(zirka)
    }
    document.write(`<br>`)
}
document.write(`<br>`);


for (let i = 0; i < 10; i++ ) {
    for(let j = i + 1 ; j < 10 ; j++){
        document.write(probil + probil) 
    }
    for(let y = i + 1 ; y  > 0  ; y--){
        document.write(zirka)
    }
    document.write(`<br>`)
}
document.write(`<br>`);


for (let i = 0; i < 10; i++ ) {
    for(let y = i + 1 ; y  > 0  ; y--){
        document.write(zirka)
    }
    document.write(`<br>`)
}
document.write(`<br>`);


for (let i = 0; i < 20 ; i++ ) {
    if( i < 10 ) {
        for(let j = i + 1 ; j < 20 ; j++){
            document.write(probil) 
        }
        for(let y = i + 1 ; y  > 0  ; y--){
            document.write(zirka)
        }
        document.write(`<br>`)
    }
    if( i == 10 || i > 10 ){
        for(let j = i + 1 ; j > 0 ; j--){
            document.write(probil) 
        }
        for(let y = i + 1 ; y < 20  ; y++){
            document.write(zirka)
        }
        document.write(`<br>`)
    }
}
document.write(`<br>`);